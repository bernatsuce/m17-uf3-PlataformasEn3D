Risketos Mínims
-3D
El nostre projecte és completament 3D

-Ha d’haver càmara 3D, que no pot ser fixa, i hauria de ser en tercera persona
Tenim una cámara 3D en Tercera persona. Es troba al prefab Camera Holder

-Ha d’haver com a mínim una font d’il·luminació complexa, i varies en general. 
Hay iluminació complexa en los 2 mapas, varias en el Map1 i un LightMap en el Map1

-Ha d’haver moviment en les tres dimensions
El nostre personatge te moviment en 3D

-Ha d’haver físiques 3D
El nostre personatge te Físiques 3D

Ha d’haver un sistema rotacional
La nostra camará té un sistema rotacional

-Pot ser un plataforma 3D. En aquest cas el moviment ha de ser lliure en 3D. No feu armes de projectils.
El nostre joc és un Plataforma 3D amb Obstacles, no fem armes de projectils pero hi ha un canó que dispara bales per a molestar i fa d’obstacle

-S’ha de fer servir raycasts
El nostre personatge utilitzem un raycast per utilitzar una palanca i obrir una porta

Risketos Addicionals
-LightMaps
Hay Lightmaps en el mapa numero 1

-Joints i aquest tipus de coses
El nostre personatge te Character Joints per al seu moviment

-Materials3D
Tenim el mapa omplert de materials, un dels que més utilitzem és el que es diu “Wood”, per a les caixes de les plataformes, que li dóna un toc únic 

-Ragdoll
Aquesta no está en la llista però és un extra interessant que ja el tingui i encara no s'ha explicat


